@ECHO off
CALL :build
GOTO:eof
:build
FOR  /r  %%L IN (*.sln) DO (
	echo Doing  %%L ...........
	call SonarScanner.MSBuild.exe begin /k:"sonarqube-test" /d:sonar.login="27cb187ff4abb19b2c6e881e416ba7148dd406d3" /d:sonar.analysis.mode=publish 

	MSBuild.exe /t:Rebuild

	call SonarScanner.MSBuild.exe end /d:sonar.login="27cb187ff4abb19b2c6e881e416ba7148dd406d3"
)
EXIT /B ```



REM SonarScanner.MSBuild.exe begin /k:"sonarqube-test" /d:sonar.login="27cb187ff4abb19b2c6e881e416ba7148dd406d3"
REM MSBuild.exe /t:Rebuild
REM SonarScanner.MSBuild.exe end /d:sonar.login="27cb187ff4abb19b2c6e881e416ba7148dd406d3"