func @_ConsoleApp1.Program.Main$string$$$(none) -> () loc("D:\\sonarqube-test2\\ConsoleApp1\\ConsoleApp1\\Program.cs" :10 :8) {
^entry (%_args : none):
%0 = cbde.alloca none loc("D:\\sonarqube-test2\\ConsoleApp1\\ConsoleApp1\\Program.cs" :10 :25)
cbde.store %_args, %0 : memref<none> loc("D:\\sonarqube-test2\\ConsoleApp1\\ConsoleApp1\\Program.cs" :10 :25)
br ^0

^0: // SimpleBlock
// Entity from another assembly: Console
%1 = cbde.unknown : none loc("D:\\sonarqube-test2\\ConsoleApp1\\ConsoleApp1\\Program.cs" :12 :30) // "Hello World!!!" (StringLiteralExpression)
%2 = cbde.unknown : none loc("D:\\sonarqube-test2\\ConsoleApp1\\ConsoleApp1\\Program.cs" :12 :12) // Console.WriteLine("Hello World!!!") (InvocationExpression)
// Entity from another assembly: Console
%3 = cbde.unknown : none loc("D:\\sonarqube-test2\\ConsoleApp1\\ConsoleApp1\\Program.cs" :13 :12) // Console.ReadLine() (InvocationExpression)
br ^1

^1: // ExitBlock
return

}
